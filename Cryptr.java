/*
 *               Cryptr
 *
 * Cryptr is a java encryption toolset
 * that can be used to encrypt/decrypt files
 * and keys locally, allowing for files to be
 * shared securely over the world wide web
 *
 * Cryptr provides the following functions:
 *	 1. Generating a secret key
 *   2. Encrypting a file with a secret key
 *   3. Decrypting a file with a secret key
 *   4. Encrypting a secret key with a public key
 *   5. Decrypting a secret key with a private key
 *
 */
import java.nio.file.Files;
import java.nio.file.Paths;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.SecureRandom;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.crypto.spec.IvParameterSpec;
import java.util.Base64;
import java.util.Arrays;

public class Cryptr {

	static private void processFile(Cipher ci,String inFile,String outFile, int offset)
    throws javax.crypto.IllegalBlockSizeException,
           javax.crypto.BadPaddingException,
           java.io.IOException
    {
        try (FileInputStream in = new FileInputStream(inFile);
             FileOutputStream out = new FileOutputStream(outFile)) {
            byte[] ibuf = new byte[1024];
			int len;
			in.skip(offset);
            while ((len = in.read(ibuf)) != -1) {
                byte[] obuf = ci.update(ibuf, 0, len);
                if ( obuf != null ) out.write(obuf);
            }
            byte[] obuf = ci.doFinal();
            if ( obuf != null ) out.write(obuf);
        }
    }

	/**
	 * Generates an 128-bit AES secret key and writes it to a file
	 *
	 * @param  secKeyFile    name of file to store secret key
	 */
	static void generateKey(String secKeyFile) throws Exception{
		KeyGenerator kgen = KeyGenerator.getInstance("AES");
		SecretKey skey = kgen.generateKey();
		try (FileOutputStream out = new FileOutputStream(secKeyFile)) {
			byte[] keyb = skey.getEncoded();
			out.write(keyb);
		}
	}


	/**
	 * Extracts secret key from a file, generates an
	 * initialization vector, uses them to encrypt the original
	 * file, and writes an encrypted file containing the initialization
	 * vector followed by the encrypted file data
	 *
	 * @param  originalFile    name of file to encrypt
	 * @param  secKeyFile      name of file storing secret key
	 * @param  encryptedFile   name of file to write iv and encrypted file data
	 */
	static void encryptFile(String originalFile, String secKeyFile, String encryptedFile) {
		// Get the secret key from disk
		SecretKeySpec skey = null;
		try {
			byte[] keyb = Files.readAllBytes(Paths.get(secKeyFile));
			skey = new SecretKeySpec(keyb, "AES");
		} catch(IOException ex) {
			System.out.println(ex.getMessage());
			System.exit(1);
		}


		
		// Generate the initialization vector (iv)
		SecureRandom srandom = new SecureRandom();
		byte[] iv = new byte[16];
		srandom.nextBytes(iv);
		IvParameterSpec ivspec = new IvParameterSpec(iv);
		System.out.println(Arrays.toString(iv));

		Cipher ci = null;
		try {
			ci = Cipher.getInstance("AES/CBC/PKCS5Padding");
			ci.init(Cipher.ENCRYPT_MODE, skey, ivspec);
		} catch(Exception ex) {
			System.out.println(ex.getMessage());
			System.exit(1);
		}

		// Write the IV to disk
		try {
			try (FileOutputStream out = new FileOutputStream(encryptedFile)) {
				out.write(iv);
			}
		} catch(FileNotFoundException ex) {
			System.out.println(ex.getMessage());
			System.exit(1);
		} catch(IOException ex) {
			System.out.println(ex.getMessage());
			System.exit(1);
		}
		try {
			try (FileInputStream in = new FileInputStream(originalFile); FileOutputStream out = new FileOutputStream(encryptedFile)) {
				byte[] ibuf = new byte[1024];
				int len;
				while ((len = in.read(ibuf)) != -1) {
					byte[] obuf = ci.update(ibuf, 0, len);
					if ( obuf != null ) out.write(obuf);
				}
				byte[] obuf = ci.doFinal();
				if ( obuf != null ) out.write(obuf);
			}
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			System.exit(1);
		}
	}


	/**
	 * Extracts the secret key from a file, extracts the initialization vector
	 * from the beginning of the encrypted file, uses both secret key and
	 * initialization vector to decrypt the encrypted file data, and writes it to
	 * an output file
	 *
	 * @param  encryptedFile    name of file storing iv and encrypted data
	 * @param  secKeyFile	    name of file storing secret key
	 * @param  outputFile       name of file to write decrypted data to
	 */
	static void decryptFile(String encryptedFile, String secKeyFile, String outputFile) {

		// load the secret key file
		byte[] keyb =  null;
		SecretKeySpec skey =  null;
		Cipher ci = null;
		try {
			keyb = Files.readAllBytes(Paths.get(secKeyFile));
			skey = new SecretKeySpec(keyb, "AES");
			ci = Cipher.getInstance("AES/CBC/PKCS5Padding");
		} catch (Exception ex) {
			System.out.println("error line 159");
			System.out.println(ex.getMessage());
			System.exit(1);
		}



		// get initialization vector
		try {	
			FileInputStream fis = null;
			int i = 0;
			byte[] iv = new byte[16];
			try {
				fis = new FileInputStream(encryptedFile);
				i = fis.read(iv, 0, 16);
			} catch (Exception ex) {
				System.out.println("error line 174");
				System.out.println(ex.getMessage());
				System.exit(1);
			} finally {
				if (fis!=null)
					fis.close();
			}
			System.out.println(Arrays.toString(iv));
			IvParameterSpec ivspec = new IvParameterSpec(iv);
			ci.init(Cipher.DECRYPT_MODE, skey, ivspec);
			try (FileInputStream in = new FileInputStream(encryptedFile);
				FileOutputStream out = new FileOutputStream(outputFile)) {
				byte[] ibuf = new byte[1024];
				int len;
				in.skip(16);
				while ((len = in.read(ibuf)) != -1) {
					byte[] obuf = ci.update(ibuf, 0, len);
					if ( obuf != null ) {
						out.write(obuf);
						System.out.println("writing bytes");
					}
				}
				byte[] obuf = ci.doFinal();
				if ( obuf != null ) out.write(obuf);
			}
		} catch (Exception ex) {
			System.out.println("error line 195");
			System.out.println(ex.getMessage());
			System.exit(1);
		}

	}


	/**
	 * Extracts secret key from a file, encrypts a secret key file using
     * a public Key (*.der) and writes the encrypted secret key to a file
	 *
	 * @param  secKeyFile    name of file holding secret key
	 * @param  pubKeyFile    name of public key file for encryption
	 * @param  encKeyFile    name of file to write encrypted secret key
	 */
	static void encryptKey(String secKeyFile, String pubKeyFile, String encKeyFile) {

		/*   FILL HERE   */

	}


	/**
	 * Decrypts an encrypted secret key file using a private Key (*.der)
	 * and writes the decrypted secret key to a file
	 *
	 * @param  encKeyFile       name of file storing encrypted secret key
	 * @param  privKeyFile      name of private key file for decryption
	 * @param  secKeyFile       name of file to write decrypted secret key
	 */
	static void decryptKey(String encKeyFile, String privKeyFile, String secKeyFile) {

		/*   FILL HERE   */

	}


	/**
	 * Main Program Runner
	 */
	public static void main(String[] args) throws Exception{

		String func;

		if(args.length < 1) {
			func = "";
		} else {
			func = args[0];
		}

		switch(func)
		{
			case "generatekey":
				if(args.length != 2) {
					System.out.println("Invalid Arguments.");
					System.out.println("Usage: Cryptr generatekey <key output file>");
					break;
				}
				System.out.println("Generating secret key and writing it to " + args[1]);
				generateKey(args[1]);
				break;
			case "encryptfile":
				if(args.length != 4) {
					System.out.println("Invalid Arguments.");
					System.out.println("Usage: Cryptr encryptfile <file to encrypt> <secret key file> <encrypted output file>");
					break;
				}
				System.out.println("Encrypting " + args[1] + " with key " + args[2] + " to "  + args[3]);
				encryptFile(args[1], args[2], args[3]);
				break;
			case "decryptfile":
				if(args.length != 4) {
					System.out.println("Invalid Arguments.");
					System.out.println("Usage: Cryptr decryptfile <file to decrypt> <secret key file> <decrypted output file>");
					break;
				}
				System.out.println("Decrypting " + args[1] + " with key " + args[2] + " to " + args[3]);
				decryptFile(args[1], args[2], args[3]);
				break;
			case "encryptkey":
				if(args.length != 4) {
					System.out.println("Invalid Arguments.");
					System.out.println("Usage: Cryptr encryptkey <key to encrypt> <public key to encrypt with> <encrypted key file>");
					break;
				}
				System.out.println("Encrypting key file " + args[1] + " with public key file " + args[2] + " to " + args[3]);
				encryptKey(args[1], args[2], args[3]);
				break;
			case "decryptkey":
				if(args.length != 4) {
					System.out.println("Invalid Arguments.");
					System.out.println("Usage: Cryptr decryptkey <key to decrypt> <private key to decrypt with> <decrypted key file>");
					break;
				}
				System.out.println("Decrypting key file " + args[1] + " with private key file " + args[2] + " to " + args[3]);
				decryptKey(args[1], args[2], args[3]);
				break;
			default:
				System.out.println("Invalid Arguments.");
				System.out.println("Usage:");
				System.out.println("  Cryptr generatekey <key output file>");
				System.out.println("  Cryptr encryptfile <file to encrypt> <secret key file> <encrypted output file>");
				System.out.println("  Cryptr decryptfile <file to decrypt> <secret key file> <decrypted output file>");
				System.out.println("  Cryptr encryptkey <key to encrypt> <public key to encrypt with> <encrypted key file> ");
				System.out.println("  Cryptr decryptkey <key to decrypt> <private key to decrypt with> <decrypted key file>");
		}

		System.exit(0);

	}

}
